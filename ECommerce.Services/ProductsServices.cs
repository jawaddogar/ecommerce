﻿using ECommerce.Database;
using ECommerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.Services
{
    public class ProductsServices
    {
        public Product GetProduct(int ID)
         {
             using (var context = new ECContext())
             {
                 return context.Products.Find(ID);
             }
         }
 
         public List<Product> GetProducts()
         {
             using (var context = new ECContext())
             {
                 return context.Products.ToList();
             }
         }
 
         public void SaveProduct(Product product)
         {
             using (var context = new ECContext())
             {
                 context.Products.Add(product);
                 context.SaveChanges();
             }
         }
 
         public void UpdateProduct(Product product)
         {
             using (var context = new ECContext())
             {
                 context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                 context.SaveChanges();
             }
         }
         
         public void DeleteProduct(int ID)
         {
             using (var context = new ECContext())
             {
                 var product = context.Products.Find(ID);
 
                 context.Products.Remove(product);
                 context.SaveChanges();
             }
         }
    }
}
