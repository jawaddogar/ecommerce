﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECommerce.Entities;

namespace ECommerce.Database
{
    public class ECContext: DbContext,IDisposable

    {
        public ECContext() : base("ECommerceConnection")
        {
        }        
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
